/* 
    publisher.cpp prints out the timestamp for roundtrip messages
    the format is 
    [pub] timestamp <initial pub timestamp> <subscriber timestamp> <final pub timestamp>
*/

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <sstream>
#include "timestamp.h"
#include <vector>
#include <unistd.h>

extern "C" {
#include <stdint.h>
#include "periodic_task.h"
}

#ifdef OPENDDS
    #include "opendds_layer.h"
#elif defined(ZMQ)
    #include "zeromq_layer.h"
#elif defined(ORTE)
    #include "orte_layer.h"
#else
    #error Undefined Layer!!!
#endif

using namespace std;

void usage() {
  std::cout 
  #ifdef OPENDDS
  #else
    << "usage " << "argv[0]" << " [options] <pub-address> " << std::endl
  #endif
    << "-num numMessages   : Number of messages to write\n"
    << "-s messageSize     : Size of each message in bytes\n"
    << "-ns numNubscribers : number of subscribers\n"
    << std::endl;
}

int main (int argc, char *argv []) {

    size_t message_size = 1000;
    size_t roundtrip_count = 100;
    // size_t num_subscribers = 0;
    const char* bind_to_sub = "";
    struct periodic_task *pt = NULL;
    int period=200;		// 200us FIXME: Allow to change it!

    int i = 0;
    while (i < argc) {
        if (std::string(argv[i]) == "-num" && (i+1) < argc) {
            roundtrip_count = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-s" && (i+1) < argc) {
            message_size = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-p" && (i+1) < argc) {
            period = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-ns" && (i+1) < argc) {
            // num_subscribers = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-?" ) {
            usage();
            return 0;
        }
        ++i;
    }

    if (argc< 2) {
        usage();
        return 0;
    }

    // size_t message_size = atoi (argv [1]);
    // size_t roundtrip_count = atoi (argv [2]);
    bind_to_sub = argv[i-1];

    AConnection conn;
    Publisher *pub;

    conn.init(argc,argv);
    pub = conn.create_publisher("A", bind_to_sub);

    std::cerr << "[pub] Entering send loop -- "<< (int)roundtrip_count
              << " messages, size = " << (int)message_size << std::endl;

    //give time for the subscribers to initialize connection
    usleep(2e6);

    unsigned long long timestamp;
    std::stringstream log;

    // to hold the message, in this case the topic and the timestamp
    char * buf;
    buf = (char*)malloc(message_size * sizeof(char));

    if (period) pt = start_periodic_timer(0, period);
    unsigned long long start = GetTimestamp();
    // size_t received_message_size=0;
    for (size_t i = 0; i != roundtrip_count*2; i++) {
        // include timestamp in the message with offset 1 for the topic
        timestamp = GetTimestamp();
        // add \0 in the message to mark the end of the string
        sprintf(buf,"%llu", timestamp);
        //send the message
        pub->send(buf, message_size);  
        log << "[pub] timestamp\t" << timestamp << "\t"
            << buf << "\t" << GetTimestamp() << "\n";
        if (pt) wait_next_activation(pt);
    }

    std::cout << log.str();
    std::cerr << "[pub] all messages sent" << std::endl;
    unsigned long elapsed = GetTimestamp() -start;
    
    double latency = (double) elapsed / (roundtrip_count * 2.0);
    std::cerr << "[pub] elapsed: " <<elapsed << " [us]"<<std::endl;
    std::cerr << "[pub] latency: " <<latency << " [us]"<<std::endl;
    std::cerr << "[pub] DONE" << std::endl;
}
