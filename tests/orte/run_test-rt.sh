#!/bin/bash

if [ $# -lt 3 ]
  then
    echo "usage $0 <number of subscriber> <message size> <number of messages>"
    echo "e.g. $0 2 100 1000"
    exit 
fi


DIR=`pwd`/
RESULT_DIR=../result
RESULT_DIR=/dev/shm
NUM_SUBSCRIBER=$1

MESSAGE_SIZE=$2
NUM_MESSAGE=$3

IS_ORTEMANAGER_RUNNING=`pgrep ortemanager`
if [ -z "$IS_ORTEMANAGER_RUNNING" ]; then
	ortemanager&
fi


for i in `seq 1 $NUM_SUBSCRIBER`
do
	 echo $DIR/subscriber  -s $MESSAGE_SIZE  -num $NUM_MESSAGE 
	 (chrt 99 $DIR/subscriber -s $MESSAGE_SIZE -num $NUM_MESSAGE > $RESULT_DIR/orteraw_sub"$i"_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt)&
done
sleep 1
echo $DIR/publisher -s $MESSAGE_SIZE -num $NUM_MESSAGE 
chrt 98 $DIR/publisher -s $MESSAGE_SIZE -num $NUM_MESSAGE > $RESULT_DIR/orteraw_pub_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt
#\c sleep 10
#killall ZeromqPublisher ZeromqSubscriber
#pkill ortemanager;
echo "DONE"
