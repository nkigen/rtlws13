#!/bin/bash

export PATH=$PATH:.
TYPE=`ls result/ |cut -f 1 -d"_" |uniq`
for type in $TYPE
do
	echo "--------------"
	echo $type
	echo -e "message size, num_messages, avg, stdev, max, min"
	echo "--------------"

	MESSAGE_SIZE=`ls ./result/$type*_sub*_* | cut -f3 -d"_"|sort -n | uniq `
	for msgsize in $MESSAGE_SIZE
	do
		NUM_MESSAGES=`ls result/$type*sub*_"$msgsize"_* | cut -f4 -d"_"|cut -f1 -d"." |sort -n | uniq`
		for nmsg in $NUM_MESSAGES
		do
			echo -n -e $msgsize'\t'
			echo -n -e $nmsg'\t'
			# echo "test"
			grep timestamp result/"$type"_sub*_"$msgsize"_"$nmsg".txt | awk '{print $4-$3} ' | avg
		done
	done
done
