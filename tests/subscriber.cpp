/* 
    subscriber.cpp prints out the timestamp for roundtrip messages
    the format is 
    [sub] timestamp <initial pub timestamp> <subscriber timestamp>
*/

// #include "zmq.hpp"
#include <iostream>
#include "timestamp.h"
#include <sstream>
#include <unistd.h>
#include <pthread.h>
#include <cstdlib>
#include <string.h>
#include <cerrno>   

#ifdef OPENDDS
    #include "opendds_layer.h"
#elif defined(ZMQ)
    #include "zeromq_layer.h"
#elif defined(ORTE)
    #include "orte_layer.h"
#else
    #error Undefined Layer!!!
#endif

using namespace std;


pthread_mutex_t calculating = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t done = PTHREAD_COND_INITIALIZER;

unsigned int ctr;
size_t roundtrip_count;
size_t message_size;

std::stringstream   out;
AConnection conn;
Subscriber *sub;
unsigned long long start, end;

void callback(char * buf, size_t size){
    // std::cerr << "[sub] called" << std::endl;
    unsigned long long timestamp = GetTimestamp();
    if (start == 0) start = timestamp;
    if (ctr < roundtrip_count) {
        std::cout << "[sub] timestamp\t" << buf <<"\t"<< timestamp << "\n";
        end = timestamp;
    }
    ctr ++;
     // cout << "counter " << ctr <<endl;
    // std::cout << "[sub] debug "<< buf << "\t" << GetTimestamp() <<std::endl;
}

void usage() {
  std::cout 
  #ifdef OPENDDS
  #else
    << "usage " << "argv[0]" << " [options] <pub-address> " << std::endl
  #endif
    << "-num numMessages   : Number of messages to write\n"
    << "-s messageSize     : Size of each message in bytes\n"
    << "-ns numNubscribers : number of subscribers\n"
    << std::endl;
}

void *read_all_mesages(void *data)
{
        int oldtype;

        /* allow the thread to be killed at any time */
        pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);

        while (ctr<roundtrip_count){
            usleep(1e6);
        }

        std::cerr << "Time to receive: " << start << " - " << end << " = " << end - start << std::endl;
        std::cerr << "Count: " << roundtrip_count << std::endl;

        /* wake up the caller if we've completed in time */
        pthread_cond_signal(&done);
        return NULL;
}

/* note: this is not thread safe as it uses a global condition/mutex */
int do_or_timeout(struct timespec *max_wait)
{
        struct timespec abs_time;
        pthread_t tid;
        int err;

        pthread_mutex_lock(&calculating);

        /* pthread cond_timedwait expects an absolute time to wait until */
        clock_gettime(CLOCK_REALTIME, &abs_time);
        abs_time.tv_sec += max_wait->tv_sec;
        abs_time.tv_nsec += max_wait->tv_nsec;

        pthread_create(&tid, NULL, read_all_mesages, NULL);

        /* pthread_cond_timedwait can return spuriously: this should
         * be in a loop for production code
         */
        err = pthread_cond_timedwait(&done, &calculating, &abs_time);

        if (err == ETIMEDOUT)
                std::cerr << "calculation timed out\n";

        if (!err)
                pthread_mutex_unlock(&calculating);

        return err;
}

int main (int argc, char *argv []) {

    size_t message_size = 1000;
    roundtrip_count = 100;
    // size_t num_subscribers = 0;
    ctr = 0;
    int i = 0;
    while (i < argc) {
        if (std::string(argv[i]) == "-num" && (i+1) < argc) {
            roundtrip_count = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-s" && (i+1) < argc) {
            message_size = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-ns" && (i+1) < argc) {
            // num_subscribers = (size_t)atoi(argv[++i]);
        } else if (std::string(argv[i]) == "-?" ) {
            usage();
            return 0;
        }
        ++i;
    }

    if (argc< 2) {
        usage();
        return 0;
    }
    // bind_to_sub = argv [1];
    // std::cerr << "[sub] "<< i << "\t" << argv[i-1] << std::endl;
    const char* connect_to_pub = argv [i-1];

    conn.init(argc, argv);
    std::cerr <<"conn init" <<endl;
    sub = conn.create_subscriber("A", connect_to_pub);
    sub->register_callback(callback);

    struct timespec max_wait;

    memset(&max_wait, 0, sizeof(max_wait));

    /* wait at most 120 seconds */
    max_wait.tv_sec = 120;
     std::cerr << "[sub] Entering recv loop -- "<< (int)roundtrip_count 
              << " messages, size = "<< (int)message_size << std::endl;
    do_or_timeout(&max_wait);
    // sub->stop_ = 1;

    // // use string stream to save the timestamps
    // std::stringstream    out;
    // zmq::message_t msg;
    
    // char * buf;
    // buf = (char*)malloc(message_size * sizeof(char));
    // for (size_t i = 0; i != roundtrip_count; i++) {
    //     message_size = sub->recv (buf);
    //      out << "[sub] timestamp\t" << "\t" << buf << "\t" << GetTimestamp() << "\n";
    // }

    std::cout <<    out.str();
    std::cerr << "[sub] Finished receiving messages" << std::endl;
    std::cerr << "[sub] DONE" << std::endl;
    return 0;

}
