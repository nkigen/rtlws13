#!/bin/bash

if [ $# -lt 3 ]
  then
    echo "usage $0 <number of subscriber> <message size> <number of messages>"
    echo "e.g. $0 2 100 1000"
    exit 
fi


DIR=`pwd`
RESULT_DIR=../result
PUB_PORT=54321
NUM_SUBSCRIBER=$1

MESSAGE_SIZE=$2
NUM_MESSAGE=$3

for i in `seq 1 $NUM_SUBSCRIBER`
do
	SUB_PORT=$((PUB_PORT+$i))
	 echo $DIR/ZeromqSubscriber  -s $MESSAGE_SIZE  -num $NUM_MESSAGE tcp://127.0.1.1:$PUB_PORT 
	 ($DIR/ZeromqSubscriber -s $MESSAGE_SIZE -num $NUM_MESSAGE tcp://127.0.1.1:$PUB_PORT > $RESULT_DIR/zeromqraw_sub"$i"_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt)&
	# echo $SUB_PORT
	# pub_opts=$pub_opts" "tcp://127.0.1.1:$SUB_PORT
done
sleep 1
echo $DIR/ZeromqPublisher -s $MESSAGE_SIZE -num $NUM_MESSAGE tcp://127.0.1.1:$PUB_PORT $pub_opts 
$DIR/ZeromqPublisher -s $MESSAGE_SIZE -num $NUM_MESSAGE tcp://127.0.1.1:$PUB_PORT  > $RESULT_DIR/zeromqraw_pub_"$MESSAGE_SIZE"_"$NUM_MESSAGE".txt
#\c sleep 10
#killall ZeromqPublisher ZeromqSubscriber
echo "DONE"
