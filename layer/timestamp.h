#ifndef _TIMESTAMP_H_
#define _TIMESTAMP_H_ value
#include <sys/time.h>

inline unsigned long long  GetTimestamp() {
  struct timeval tv;
  gettimeofday(&tv,NULL);
    // std::cout << tv.tv_sec << " "<<tv.tv_usec<<std::endl;
  return tv.tv_sec*(unsigned long long)1000000+tv.tv_usec;
}

#endif


