/** @file RawBufferListenerImpl.h
 *  @brief This file contains the ORTE data reader listener implementation
 *  @version 0.5
*/

#ifndef RAWBUFFER_LISTENER_IMPL_H                                                                  
#define RAWBUFFER_LISTENER_IMPL_H                                                                  

#include <ace/Global_Macros.h>                                                                      

#include <dds/DdsDcpsSubscriptionC.h>                                                               
#include <dds/DCPS/LocalObject.h>                                                                   
#include <dds/DCPS/Definitions.h>                                                                   
#include <orbsvcs/Time_Utilities.h>
// #include "opendds_layer.h"

class Subscriber;

/**
 * @brief A data reader listener class
 *
 */
class RawBufferListenerImpl: public virtual OpenDDS::DCPS::LocalObject<
		DDS::DataReaderListener> {
public:
	/**
	 * @brief Constructor
	 *
	 * Attach subscriber to the listener
	 *
	 * @param sub ORTE subscriber
	 */
	RawBufferListenerImpl(Subscriber *sub);

	/**
	 * @brief Handle DDS::RequestedDeadlineMissedStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_requested_deadline_missed(DDS::DataReader_ptr reader,
			const DDS::RequestedDeadlineMissedStatus& status);

	/**
	 * @brief Handle DDS::RequestedIncompatibleQosStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_requested_incompatible_qos(DDS::DataReader_ptr reader,
			const DDS::RequestedIncompatibleQosStatus& status);

	/**
	 * @brief Handle DDS::SampleRejectedStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_sample_rejected(DDS::DataReader_ptr reader,
			const DDS::SampleRejectedStatus& status);

	/**
	 * @brief Handle DDS::LivelinessChangedStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_liveliness_changed(DDS::DataReader_ptr reader,
			const DDS::LivelinessChangedStatus& status);

	/**
	 * @brief This method is called when a new data is available
	 *
	 * This is the only method implemented.
	 * This method is called whenever a new data has been received by
	 * the subscriber.
	 * If the data is valid, it will trigger the callback method
	 *
	 * @param reader Data reader
	 */
	virtual void on_data_available(DDS::DataReader_ptr reader);

	/**
	 * @brief Handle DDS::SubscriptionMatchedStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_subscription_matched(DDS::DataReader_ptr reader,
			const DDS::SubscriptionMatchedStatus& status);

	/**
	 * @brief Handle DDS::SampleLostStatus. Not implemented
	 * @param reader Data reader
	 * @param status
	 */
	virtual void on_sample_lost(DDS::DataReader_ptr reader,
			const DDS::SampleLostStatus& status);
private:
	Subscriber *subscriber_;
};

#endif /* RAWBUFFER_LISTENER_IMPL_H */ 
