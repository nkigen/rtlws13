#include "opendds_layer.h"
#include "debug.h"   

DDS::DomainId_t MESSAGE_DOMAIN_ID = 1066;

AConnection::AConnection() :
		handle_(0) {
        dpf_ = DDS::DomainParticipantFactory::_nil();
        participant_ = DDS::DomainParticipant::_nil();
}

int AConnection::init(int argc, char *argv[]) {
	// Initialize DomainParticipantFactory

	dpf_ = TheParticipantFactoryWithArgs(argc, argv);

	// Create DomainParticipant
	participant_ = dpf_->create_participant(MESSAGE_DOMAIN_ID,
			PARTICIPANT_QOS_DEFAULT, 0, OpenDDS::DCPS::DEFAULT_STATUS_MASK);

	if (!participant_) {
		ACE_ERROR_RETURN((LM_ERROR,
					ACE_TEXT("ERROR: %N:%l: main() -")
					ACE_TEXT(" create_participant failed!\n")),
			-1);
	}
	return 0;
}

AConnection::~AConnection() {
    // Clean-up!
    if (participant_)
        participant_->delete_contained_entities();
    dpf_->delete_participant(participant_);
    TheServiceParticipant->shutdown();
}

Publisher::Publisher() {
}
Publisher::~Publisher(){}

Publisher * AConnection::create_publisher(std::string topic, std::string address) {
    Publisher * pub = new Publisher();
    pub->topic_ = topic;
    // Register TypeSupport (Message::RawBuffer)
    pub->rawbuffer_ts_ = new Message::RawBufferTypeSupportImpl;
    if (pub->rawbuffer_ts_->register_type(participant_, "") != DDS::RETCODE_OK) {
        // err
    }
    PDEBUG("[pub] Topic");
     // Create Topic (Movie Discussion List)
    CORBA::String_var type_name = pub->rawbuffer_ts_->get_type_name();
    pub->rawbuffer_topic_ = participant_->create_topic(pub->topic_.c_str(), type_name,
    	TOPIC_QOS_DEFAULT, 0, OpenDDS::DCPS::DEFAULT_STATUS_MASK);
    if (!pub->rawbuffer_topic_) {
        //err
    }
    PDEBUG("[pub] participant");
    pub->publisher_ = participant_->create_publisher(PUBLISHER_QOS_DEFAULT, 0,
    OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!pub->publisher_) {
        //err
    }

    PDEBUG("[pub] dw");
     // Create DataWriter
    pub->rawbuffer_base_dw_ = pub->publisher_->create_datawriter(pub->rawbuffer_topic_,
        DATAWRITER_QOS_DEFAULT, //TODO 
        0, OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!pub->rawbuffer_base_dw_) {
        //err
    }
    PDEBUG("[pub] narrow");
    pub->rawbuffer_dw_ = Message::RawBufferDataWriter::_narrow(pub->rawbuffer_base_dw_);
    if (!pub->rawbuffer_dw_) {
        //err
    }
    PDEBUG("PUB INIT");
    return pub;
}

void Publisher::send(const char * buf, size_t message_size){
    // PDEBUG("before copy");
    Message::RawBuffer message;
    // message.buffer.length(message_size);
    // memcpy(message.buffer.get_buffer(), buf, message_size);
    message.buffer = Message::BufferType (message_size,message_size,
      (::CORBA::Octet*)buf);
    // PDEBUG("after copy");
    // message.buffer.length(message_size);
    // message.buffer = buf;
    DDS::ReturnCode_t error = rawbuffer_dw_->write(message, DDS::HANDLE_NIL);
    if (error != DDS::RETCODE_OK)
        std::cerr <<"[pub] error " << error <<"\t" << DDS::RETCODE_OK << std::endl;
    // PDEBUG("message sent");
    // if (error != DDS::RETCODE_OK) {
    //     ACE_ERROR((LM_ERROR,
    //                ACE_TEXT("ERROR: %N:%l: main() -")
    //                ACE_TEXT(" write returned %d!\n"), error));
    //   }
// return 0;
}

Subscriber::Subscriber(){
    is_new_message_arrived_ = 0;
    recv_callback_ = NULL;
}
Subscriber::~Subscriber(){};
Subscriber * AConnection::create_subscriber(std::string topic,
    std::string address){

    Subscriber * sub = new Subscriber();
    sub->topic_ = topic;

    // Register TypeSupport (Message::RawBuffer)
    sub->rawbuffer_ts_ = new Message::RawBufferTypeSupportImpl;
    if (sub->rawbuffer_ts_->register_type(participant_, "") != DDS::RETCODE_OK) {
       //err
    }
     // Create Topic (Movie Discussion List)
    CORBA::String_var type_name = sub->rawbuffer_ts_->get_type_name();
    sub->rawbuffer_topic_ = participant_->create_topic(sub->topic_.c_str(), type_name,
        TOPIC_QOS_DEFAULT, 0, OpenDDS::DCPS::DEFAULT_STATUS_MASK);
    if (!sub->rawbuffer_topic_) {
        //err
    }
    // Create Subscriber
    sub->subscriber_ = participant_->create_subscriber(SUBSCRIBER_QOS_DEFAULT,
                                   0,
                                   OpenDDS::DCPS::DEFAULT_STATUS_MASK);

    if (!sub->subscriber_) {
        //err
    }


    // Create DataReader
    sub->rawbuffer_listener_ = new RawBufferListenerImpl(sub);
    // sub->rawbuffer_listener_->set_subscriber(this);
    sub->rawbuffer_base_dr_ = sub->subscriber_->create_datareader(sub->rawbuffer_topic_,
                           DATAREADER_QOS_DEFAULT,
                           sub->rawbuffer_listener_,
                           OpenDDS::DCPS::DEFAULT_STATUS_MASK);
    if (!sub->rawbuffer_base_dr_) {
        //err
    }
    return sub;
}

void Subscriber::register_callback(void (*fun)(char* buf, size_t size)){
    recv_callback_ =  fun;
}

int Subscriber::recv(char * buf){
    // TODO unimplemented
    return 0;
}

// int Publisher::wait_for_subscriber(){
//   // Block until Subscriber is available
//   DDS::StatusCondition_var condition = rawbuffer_base_dw_->get_statuscondition();
//   condition->set_enabled_statuses(DDS::PUBLICATION_MATCHED_STATUS);

//   DDS::WaitSet_var ws = new DDS::WaitSet;
//   ws->attach_condition(condition);

//   while (true) {
//     DDS::PublicationMatchedStatus matches;
//     if (rawbuffer_base_dw_->get_publication_matched_status(matches) != ::DDS::RETCODE_OK) {
//       ACE_ERROR_RETURN((LM_ERROR,
//                         ACE_TEXT("ERROR: %N:%l: main() -")
//                         ACE_TEXT(" get_publication_matched_status failed!\n")),
//                        -1);
//     }

//     if (matches.current_count >= 1) {
//       break;
//     }

//     DDS::ConditionSeq conditions;
//     DDS::Duration_t timeout = { 60, 0 };
//     if (ws->wait(conditions, timeout) != DDS::RETCODE_OK) {
//       ACE_ERROR_RETURN((LM_ERROR,
//                         ACE_TEXT("ERROR: %N:%l: main() -")
//                         ACE_TEXT(" wait failed!\n")),
//                        -1);
//     }
//   }

//   ws->detach_condition(condition);
//   return 1;
// }
