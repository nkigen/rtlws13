/** @file orte_layer.h
 *  @brief This file contains the ORTE layer concrete class for connection, publisher and subscriber
 *  @version 0.5
*/

#ifndef _ORTE_LAYER_
#define _ORTE_LAYER_ 

#include <string>
#include <vector>
#include "baselayer.h"
#include "orte.h"

namespace orte_layer {
class Publisher;
class Subscriber;


/**
 * @brief A concrete class for the publisher class for ORTE.
 *
 * This class serves as an API wrapper class for ORTE publisher socket.
 */
class AConnection:public BaseAConnection{
	friend class Publisher;
	friend class Subscriber;
public:
	/**
	 * @brief Default constructor
	 */
    AConnection();
    /**
     * @brief Default destructor
     */
    virtual ~AConnection();

    /**
	 * Initialize the ORTE and create the ORTE domain
	 * @param argc The number of arguments. Set to 0
	 * @param argv The array of arguments. Set to NULL
	 * @return
	 */
    int init(int argc, char * argv[]);
    /**
	 * @brief Create an ORTE subscriber.
	 *
	 * This method create an ORTE subscriber and initialize the callback thread.
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the subscriber
	 */
    Subscriber *create_subscriber(std::string topic, std::string address);

    /**
	 * @brief Create an ORTE publisher.
	 *
	 * This method create an ORTE publisher.
	 *
	 * @param topic Topic filter
	 * @param address The publisher network address
	 * @return the publisher
	 */
    Publisher *create_publisher(std::string topic, std::string address);

    /**
     * ORTE domain object
     */
    ORTEDomain      *domain_;
private:
	
};

/**
 * @brief A concrete class for the publisher class for ORTE.
 *
 * This class serves as an API wrapper class for ORTE publisher.
 */
class Publisher:public BasePublisher{
	friend class AConnection;
public:
	/**
	 * @brief Default constructor
	 */
	Publisher();

	/**
	 * @brief Default destructor
	 */
	virtual ~Publisher();

	/**
	 * @brief Send a packet to all subscriber
	 *
	 * This method send a packet using ORTE publisher to all its subscribers
	 * using a blocking send method.
	 * The buffer to be sent is copied to the internal sendbuffer.
	 * The maximum message size is 10000 bytes
	 *
	 * @param buf The message
	 * @param message_size The size of the message
	 */
	void send(const char * buf,size_t message_size);

	/**
	 * ORTE publication object
	 */
	ORTEPublication     *publisher_;
private:
	std::string 		address_;
//	std::string 		topic_;
	std::string			typename_;
//	size_t 				toffset_;

	
	NtpTime         	persistence_;
	NtpTime				delay_;

	static const unsigned int   	MAXBUFFER = 10000;
	char            	sendbuffer_[Publisher::MAXBUFFER];


	
};


/**
 * @brief The concrete class for the subscriber class for ORTE
 *
 * This class serves as an API wrapper class for ORTE subscriber.
 */
class Subscriber:public BaseSubscriber{
	friend class AConnection;
public:
	/**
	 * @brief Default constructor
	 */
	Subscriber();

	/**
	 * @brief Default destructor
	 */
	virtual ~Subscriber();

	/**
	 * @brief A blocking receive method
	 * @warning do not use this function. Look at register_callback() instead.
	 *
	 * @param buf The received message. This method expect that he array has been preallocated
	 * @return the size of the message received
	 */
	int recv(char * buf);

	/**
	 * @brief Register a function that is called whenever the subscriber receive a message
	 *
	 * This method registered a user defined function that is called when a message received.
	 * The subscriber object has and internal thread that listens for incoming packets.
	 * The thread trigger the callback function.
	 * It provides a non-blocking mechanism to receive a packet.
	 *
	 * @param fun The user defined callback function
	 */
	void register_callback(void (*fun)(char*,size_t));

private:
	void (* recv_callback_)(char*,size_t);


	SubscriptionMode subscription_mode_;
	SubscriptionType subscription_type_;

	std::string 		address_;
//	std::string 		topic_;
	std::string			typename_;
	size_t 				toffset_;

	ORTESubscription    *subscriber_;
  	NtpTime             deadline_;
  	NtpTime				minimum_separation_;
	pthread_t 			listener_;

	static const int   	MAXBUFFER = 5000;
	char        		recvbuffer_[Subscriber::MAXBUFFER];

	// void recv_callback_(const ORTERecvInfo *info,void *vinstance, void *recvCallBackParam);
};

}

#endif
