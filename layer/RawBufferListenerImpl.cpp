

#include <ace/Log_Msg.h>
#include <ace/OS_NS_stdlib.h>

#include "RawBufferListenerImpl.h"
#include "MessageTypeSupportC.h"
#include "MessageTypeSupportImpl.h"
#include <boost/thread/mutex.hpp>

#include <iostream>
#include "timestamp.h"
#include "opendds_layer.h"

RawBufferListenerImpl::RawBufferListenerImpl(Subscriber *subscriber){
    subscriber_ =  subscriber;
}

void RawBufferListenerImpl::on_requested_deadline_missed(
    DDS::DataReader_ptr /*reader*/,
    const DDS::RequestedDeadlineMissedStatus& /*status*/) {
}

void RawBufferListenerImpl::on_requested_incompatible_qos(
    DDS::DataReader_ptr /*reader*/,
    const DDS::RequestedIncompatibleQosStatus& /*status*/) {
}

void RawBufferListenerImpl::on_sample_rejected(
    DDS::DataReader_ptr /*reader*/,
    const DDS::SampleRejectedStatus& /*status*/){
}

void RawBufferListenerImpl::on_liveliness_changed(
    DDS::DataReader_ptr /*reader*/,
    const DDS::LivelinessChangedStatus& /*status*/) {
}


void RawBufferListenerImpl::on_data_available(DDS::DataReader_ptr reader) {
    Message::RawBufferDataReader_var reader_i =
        Message::RawBufferDataReader::_narrow(reader);

    if (!reader_i) {
        ACE_ERROR((LM_ERROR,
                   ACE_TEXT("ERROR: %N:%l: on_data_available() -")
                   ACE_TEXT(" _narrow failed!\n")));
        ACE_OS::exit(-1);
    }

    Message::RawBuffer message;
    DDS::SampleInfo info;

    DDS::ReturnCode_t error = reader_i->take_next_sample(message, info);

    if (error == DDS::RETCODE_OK) {
        // std::cout << "SampleInfo.sample_rank = " << info.sample_rank << std::endl;
        // std::cout << "SampleInfo.instance_state = " << info.instance_state << std::endl;

        if (info.valid_data) {
            // TimeBase::TimeT end = get_timestamp();
            // // std::cout << "[rawbuffer] latency = "<< end - message.timestamp << std::endl; 
            size_t message_size = message.buffer.length();
            char * buf;
            // buf = (char *)malloc(message_size * sizeof(char));
            // memcpy(buf,  message.buffer.get_buffer(), message.buffer.length());
            
            // std::cout << "[sub] timestamp\t" << message.buffer.get_buffer() <<"\t"<< (unsigned long long) GetTimestamp() << "\n";

            if (subscriber_->recv_callback_){
                // std::cout << "callback registered" <<std::endl;
                // subscriber_->recv_callback_(buf, message_size);
                subscriber_->recv_callback_((char *)(message.buffer.get_buffer()), message_size);
            }

            // boost::mutex::scoped_lock lock(*(subscriber_->recv_mutex_));
            // subscriber_->message_ = buf;
            // subscriber_->message_size_ = message_size;
            // subscriber_->is_new_message_arrived_ = 1;
            
            // free(buf);

            

            // std::cout << "buffer contents " << message.buffer.length() 
            //     <<"\t"<< (unsigned int)message.buffer[0] << "\t" << (unsigned int)message.buffer[message.buffer.length()-1] << std::endl;
            // std::cout<< "message received correctly " << std::endl;
        }

    } else {
        ACE_ERROR((LM_ERROR,
                   ACE_TEXT("ERROR: %N:%l: on_data_available() -")
                   ACE_TEXT(" take_next_sample failed!\n")));
    }
}

void RawBufferListenerImpl::on_subscription_matched(
    DDS::DataReader_ptr /*reader*/,
    const DDS::SubscriptionMatchedStatus& /*status*/) {
}

void RawBufferListenerImpl::on_sample_lost(
    DDS::DataReader_ptr /*reader*/,
    const DDS::SampleLostStatus& /*status*/) {
}
